version="0.2"
tags={
	"Alternative History"
	"National Focuses"
}
name="Failurereich: Stalemate"
replace_path="common/national_focus"
replace_path="common/decisions"
replace_path="common/ai_strategy_plans"
replace_path="events"
picture="thumbnail.png"
supported_version="1.10.5"
remote_file_id="2077121121"